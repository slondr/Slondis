%{
void comment();
void count();
%}

%option noyywrap

%%

\(                        { count(); return(LEFTPAREN); }
\)                        { count(); return(RIGHTPAREN); }
[a-zA-Z\-]*               { count(); return(IDENTIFIER); }
[0-9]*                    { count(); return(NUMBER); }
\"(([^\"]|\\\")*[^\\])?\" { count(); return(STRING); }
\[\[                      { comment(); }

%%

int column = 0;
void count() {
  for(int i = 0; yytext[i] != '\0'; i++) {
    if(yytext[i] == '\n') {
      column = 0;
    } else if(yytext[i] == '\t') {
      column += 4 - (column % 4);
    } else {
      column++;
    }
  }
  ECHO;
}

void comment() {
  char c, c1;
 loop:
  while((c = input()) != ']' && c != 0)
    putchar(c);
  
  if((c1 = input()) != ']' && c != 0) {
    unput(c1);
    goto loop;
  }
  
  if(c != 0)
    putchar(c1);
}

/*
int main(void) {
  yylex();
  return 0;
}

*/
