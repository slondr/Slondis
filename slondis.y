%token LEFTPAREN
%token RIGHTPAREN
%token IDENTIFIER
%token NUMBER
%token STRING

%%
primary_expression: IDENTIFIER | STRING;
%%
#include <stdio.h>
#include "lex.yy.c"
extern char * yytext;
extern int column;

void yyerror(char * s) {
  fflush(stdout);
  printf("\n%*s\n%*s\n", column, "^", column, s);
}

int main() {
  return yyparse();
}

