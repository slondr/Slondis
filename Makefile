build:
	flex slondis.lex
	bison slondis.y
	gcc -o slondis slondis.tab.c

clean:
	rm -f lex.yy.c
	rm -f slondis.tab.c
	rm -f slondis
